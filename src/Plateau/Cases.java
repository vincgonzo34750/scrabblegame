package Plateau;

public class Cases {
    private int coordX;
    private int coordY;
    private boolean isSpecial;
    private enum Special {
        LETTRE_COMPTE_DOUBLE,
        LETTRE_COMPTE_TRIPLE,
        MOT_COMPTE_DOUBLE,
        MOT_COMPTE_TRIPLE;
    }

    Cases(int coordX, int coordY){
        this.coordX = coordX;
        this.coordY = coordY;
    }
}
